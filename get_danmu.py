import requests
import re
import traceback
import sys


def req_url(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36',

    }
    res = requests.get(url, headers=headers)

    return res


# 根据BV号获取CID
def get_cid(number):
    """
    :param number:B站视频的BV号
    :return:对应的CID
    """
    url = f'https://api.bilibili.com/x/player/pagelist?bvid={number}&jsonp=jsonp'
    cid_list = []

    res = req_url(url)
    res_json = res.json()
    for item in res_json['data']:
        cid_list.append(item['cid'])
    return cid_list


# 根据CID获取弹幕
def get_danmu_by_cid(cid_list):
    all_danmu_list = []
    for cid in cid_list:
        url = f'https://api.bilibili.com/x/v1/dm/list.so?oid={cid}'
        res = req_url(url)
        res_xml = res.content.decode(encoding='utf8')
        pattern = re.compile('<d.*?>(.*?)</d>')
        danmu_list = pattern.findall(res_xml)
        all_danmu_list.append(danmu_list)

    return all_danmu_list


# 保存弹幕
def save_danmu_to_file(all_danmu_list, bv):
    file_name = f'{bv}.txt'
    with open(file_name, mode='w', encoding='utf8') as f:
        i = 0
        for danmu_list in all_danmu_list:
            for item in danmu_list:
                i += 1
                f.write(item)
                f.write('\n')
        print(f'获取到弹幕{i}条')


def main(bv_number):
    try:
        if type(bv_number) != str:
            print('格式有误')
            sys.exit(0)
        elif len(bv_number) == 0:
            print('输入为空')
            sys.exit(0)
        else:
            cid_list = get_cid(bv_number)
            all_danmu_list = get_danmu_by_cid(cid_list)
            save_danmu_to_file(all_danmu_list, bv_number)
    except Exception:
        print(traceback.format_exc())


if __name__ == '__main__':
    av = input('请输入要查询弹幕列表的BV号，格式：avXXXXXXXX：')
    main(av)

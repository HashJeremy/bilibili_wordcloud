import get_danmu
import gen_wordcloud


def run():
    bv_number = input('请输入要查询弹幕列表的视频BV号，格式：XXXXXXXX：')
    get_danmu.main(bv_number)
    gen_wordcloud.main(bv_number)


run()

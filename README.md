# bilibili_wordcloud

#### 介绍
指定哔哩哔哩上视频的BV号可以获取该视频下所有分P的所有弹幕，并生成词云

#### 软件架构
+ Python 3.8
+ jieba 0.42.1
+ wordcloud 1.6

#### 特别说明
- wordcloud字体请选择对应操作系统平台拥有字体（避免出现OSError: cannot open resource）
- Windows可以选择msyh.ttc
- macOS可以选择/System/Library/fonts/PingFang.ttc